import QtQuick 2.12
import gitSample 1.0

Item {
    id: btn_lb01
    width: 600
    height: 40
    property alias txt_s: text_001_217_56.text
    property alias txt_p: text_001_217_55.text
    property alias txt_n: text_001_217_61.text
    Item {
        id: img_lb01_s
        x: 0
        y: 0
        width: 600
        height: 40
        visible: false
        Image {
            id: img_lb01_bg_p_217_55
            x: 0
            y: 0
            source: "assets/img_lb01_bg_s.png"
        }

        Item {
            id: txt_lb01_s
            x: 10
            y: 14
            width: 56
            height: 13
            Text {
                id: text_001_217_56
                x: 0
                y: -1
                color: "#1093de"
                text: "Menu"
                font.family: "ＭＳ Ｐゴシック"
                font.pixelSize: 17
            }
        }
    }

    Item {
        id: img_lb01_p
        x: 0
        y: 0
        width: 600
        height: 40
        visible: true
        Image {
            id: img_lb01_bg_p
            x: 0
            y: 0
            source: "assets/img_lb01_bg_p_217_54.png"
        }

        Item {
            id: txt_lb01_p
            x: 10
            y: 14
            width: 56
            height: 13
            Text {
                id: text_001_217_55
                x: 0
                y: -1
                color: "#FFFFFF"
                text: "Menu"
                font.pixelSize: 17
                font.family: "ＭＳ Ｐゴシック"
            }
        }
    }

    Item {
        id: img_lb01_n
        x: 0
        y: 0
        width: 600
        height: 40
        Image {
            id: img_lb01_bg_n
            x: 0
            y: 0
            source: "assets/img_lb01_bg_n_217_60.png"
        }

        Item {
            id: txt_lb01_n
            x: 10
            y: 14
            width: 56
            height: 13
            Text {
                id: text_001_217_61
                x: 0
                y: -1
                color: "#E6E6E6"
                text: "Menu"
                font.pixelSize: 17
                font.family: "ＭＳ Ｐゴシック"
            }
        }
    }

    MouseArea {
        id: btn_lb01_area
        anchors.fill: parent
    }
    Connections {
        target: btn_lb01_area
        onReleased: img_lb01_n.visible = true
    }
    Connections {
        target: btn_lb01_area
        onPressAndHold: img_lb01_n.visible = false
    }
    Connections {
        target: btn_lb01_area
        onPressAndHold: img_lb01_p.visible = false
    }
    Connections {
        target: btn_lb01_area
        onPressAndHold: img_lb01_s.visible = true
    }
    Connections {
        target: btn_lb01_area
        onReleased: img_lb01_p.visible = true
    }

    Connections {
        target: btn_lb01_area
        onPressed: img_lb01_n.visible = false
    }
}
