import QtQuick 2.12
import gitSample 1.0
import QtQuick.Controls 2.12
import QtQuick.Timeline 1.0

Item {
    id: root
    width: Constants.width
    height: Constants.height
    Rectangle {
        id: rectangle
        width: 1024
        height: 800
        color: "#000000"

        Text {
            id: element
            x: 288
            y: 70
            color: "#ffffff"
            text: qsTr("Scrollは[ScrollView]を使用")
            opacity: 1
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 40
        }

        Text {
            id: element1
            x: 103
            y: 212
            color: "#ffffff"
            text: qsTr("データ「Scrolldata.ui.qml」")
            opacity: 1
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 20
        }

        Scr_lb {
            id: scr_lb
            x: 367
            y: 387
        }

        Text {
            id: element2
            x: 144
            y: 244
            color: "#ffffff"
            text: qsTr("PNG画像をスクロール")
            horizontalAlignment: Text.AlignHCenter
            opacity: 1
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 14
        }

        Text {
            id: element3
            x: 121
            y: 448
            color: "#ffffff"
            text: qsTr("データ「Scr_lb.ui.qml」")
            horizontalAlignment: Text.AlignHCenter
            opacity: 1
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 20
        }

        Text {
            id: element4
            x: 92
            y: 484
            color: "#ffffff"
            text: qsTr("縦方向に配置したコンポーネントをスクロール")
            horizontalAlignment: Text.AlignHCenter
            opacity: 1
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 14
        }
    }

    Scrolldata {
        x: 381
        y: 140
    }
}
