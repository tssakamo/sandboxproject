import QtQuick 2.12
import gitSample 1.0

Item {
    id: artboard
    width: 640
    height: 180

    Item {
        id: img_scr_base
        x: 0
        y: 0
        width: 640
        height: 180
        Image {
            id: img_scr_bg
            x: 0
            y: 0
            source: "assets/img_scr_bg_217_2.png"
        }
    }

    Item {
        id: btn_lb04
        x: 20
        y: 136
        width: 600
        height: 40

        Btn_parts {
            id: btn_parts4
            x: 0
            y: 0
            txt_n: "Button 4"
            txt_p: "Button 4  <クリックでpush状態、押しっぱなしでselect、放して元に戻る>"
            txt_s: "Button 4"
        }
    }

    Item {
        id: btn_lb03
        x: 20
        y: 92
        width: 600
        height: 40

        Btn_parts {
            id: btn_parts3
            x: 0
            y: 0
            txt_n: "Button 3"
            txt_p: "Button 3  <クリックでpush状態、押しっぱなしでselect、放して元に戻る>"
            txt_s: "Button 3"
        }
    }

    Item {
        id: btn_lb02
        x: 20
        y: 48
        width: 600
        height: 40

        Btn_parts {
            id: btn_parts2
            x: 0
            y: 0
            txt_n: "Button 2"
            txt_p: "Button 2  <クリックでpush状態、押しっぱなしでselect、放して元に戻る>"
            txt_s: "Button 2"
        }
    }
    Item {
        id: btn_lb01
        x: 20
        y: 4
        width: 600
        height: 40

        Btn_parts {
            id: btn_parts1
            x: 0
            y: 0
            txt_n: "Button 1"
            txt_p: "Button 1  <クリックでpush状態、押しっぱなしでselect、放して元に戻る>"
            txt_s: "Button 1"
        }
    }
}

/*##^##
Designer {
    D{i:0;UUID:"9667ca2f37f47d4a36f8534638ecc8a3"}
}
##^##*/

