import QtQuick 2.12
import gitSample 1.0

Item {
    width: Constants.width
    height: Constants.height

    Rectangle {
        id: rectangle
        x: 0
        y: 0
        width: 1024
        height: 800
        color: "#42b237"

        Text {
            id: element
            x: 431
            y: 172
            width: 196
            height: 91
            color: "#e0d7d7"
            text: qsTr("Text3_sei")
            font.styleName: "Medium"
            font.family: "Avenir Next"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 60
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.33000001311302185}
}
##^##*/

