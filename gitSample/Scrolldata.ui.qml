import QtQuick 2.12
import gitSample 1.0
import QtQuick.Controls 2.12

Item {
    width: 400
    height: 200
    Rectangle {
        id: rectangle
        width: 400
        height: 200
        color: "#000000"
    }

    ScrollView {
        anchors.fill: parent
        clip: true
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOn

        Item {
            height: 200
            width: image001.width + image002.width + image003.width
            implicitWidth: width

            Image {
                id: image001
                height: 200
                fillMode: Image.PreserveAspectFit
                source: "assets/img_scr001b_h.png"
            }

            Image {
                id: image002
                x: 600
                height: 200
                fillMode: Image.PreserveAspectFit
                source: "assets/img_scr002_h.png"
            }

            Image {
                id: image003
                x: 1200
                height: 200
                fillMode: Image.PreserveAspectFit
                source: "assets/img_scr003_h.png"
            }
        }
    }
}
