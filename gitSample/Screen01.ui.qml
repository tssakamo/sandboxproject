import QtQuick 2.12
import gitSample 1.0
import QtQuick.Timeline 1.0

Rectangle {
    width: Constants.width
    height: Constants.height

    color: "#000000"

    Text {
        id: main_txt
        text: qsTr("gitSample")
        font.styleName: "Medium"
        anchors.verticalCenterOffset: 171
        anchors.horizontalCenterOffset: 1
        font.family: "Avenir Next"
        font.pointSize: 24
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
        visible: true
        anchors.centerIn: parent
    }

    Timeline {
        id: timeline
        animations: [
            TimelineAnimation {
                id: timelineAnimation
                running: true
                from: 0
                to: 5000
                loops: -1
                duration: 5000
            }
        ]
        endFrame: 5000
        startFrame: 0
        enabled: true

        KeyframeGroup {
            target: main_txt
            property: "opacity"
            Keyframe {
                frame: 0
                value: 0
            }

            Keyframe {
                frame: 2000
                value: 1
            }
        }

        KeyframeGroup {
            target: main_txt
            property: "color"
            Keyframe {
                value: "#000000"
                frame: 2000
            }
            Keyframe {
                value: "#ff0000"
                frame: 4000
            }
        }

        KeyframeGroup {
            target: swipe_txt
            property: "opacity"
            Keyframe {
                frame: 0
                value: 0
            }

            Keyframe {
                frame: 2769
                value: 0
            }

            Keyframe {
                frame: 3825
                value: 1
            }
        }
    }

    Image {
        id: image
        x: 392
        y: 262
        width: 241
        height: 190
        fillMode: Image.PreserveAspectFit
        source: "assets/qt_logo.png"
    }

    Text {
        id: swipe_txt
        x: 801
        y: 322
        color: "#ffffff"
        text: qsTr("Swipe")
        style: Text.Normal
        font.styleName: "Medium"
        font.weight: Font.ExtraLight
        font.family: "Avenir Next"
        opacity: 1
        font.pixelSize: 48
    }
}

/*##^##
Designer {
    D{i:2}D{i:15;timeline_expanded:true}
}
##^##*/

