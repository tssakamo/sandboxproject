import QtQuick 2.12
import gitSample 1.0
import QtQuick.Controls 2.12

Item {
    width: 640
    height: 180

    ScrollView {
        anchors.fill: parent
        clip: true
        ScrollBar.vertical.policy: ScrollBar.AlwaysOn

        Item {
            width: 640
            height: 560
            implicitHeight: height

            Btn_parts {
                id: scrbtn01
                x: 20
                y: 8
                txt_n: "List Button 01"
                txt_p: "List Button 01  <push>"
                txt_s: "List Button 01"
            }

            Btn_parts {
                id: scrbtn02
                x: 20
                y: 54
                txt_n: "List Button 02"
                txt_p: "List Button 02  <push>"
                txt_s: "List Button 02"
            }

            Btn_parts {
                id: scrbtn03
                x: 20
                y: 100
                txt_n: "List Button 03"
                txt_p: "List Button 03  <push>"
                txt_s: "List Button 03"
            }

            Btn_parts {
                id: scrbtn04
                x: 20
                y: 146
                txt_n: "List Button 04"
                txt_p: "List Button 04  <push>"
                txt_s: "List Button 04"
            }

            Btn_parts {
                id: scrbtn05
                x: 20
                y: 192
                txt_n: "List Button 05"
                txt_p: "List Button 05  <push>"
                txt_s: "List Button 05"
            }

            Btn_parts {
                id: scrbtn06
                x: 20
                y: 238
                txt_n: "List Button 06"
                txt_p: "List Button 06  <push>"
                txt_s: "List Button 06"
            }

            Btn_parts {
                id: scrbtn07
                x: 20
                y: 284
                txt_n: "List Button 07"
                txt_p: "List Button 07  <push>"
                txt_s: "List Button 07"
            }

            Btn_parts {
                id: scrbtn08
                x: 20
                y: 330
                txt_n: "List Button 08"
                txt_p: "List Button 08  <push>"
                txt_s: "List Button 08"
            }

            Btn_parts {
                id: scrbtn09
                x: 20
                y: 377
                txt_n: "List Button 09"
                txt_p: "List Button 09  <push>"
                txt_s: "List Button 09"
            }

            Btn_parts {
                id: scrbtn10
                x: 20
                y: 423
                txt_n: "List Button 10"
                txt_p: "List Button 10  <push>"
                txt_s: "List Button 10"
            }

            Btn_parts {
                id: scrbtn11
                x: 20
                y: 469
                txt_n: "List Button 11"
                txt_p: "List Button 11  <push>"
                txt_s: "List Button 11"
            }

            Btn_parts {
                id: scrbtn12
                x: 20
                y: 515
                txt_n: "List Button 12"
                txt_p: "List Button 12  <push>"
                txt_s: "List Button 12"
            }
        }
    }
}
