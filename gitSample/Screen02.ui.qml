import QtQuick 2.12
import gitSample 1.0

Item {
    id: root
    width: Constants.width
    height: Constants.height

    Rectangle {
        id: rectangle
        width: 1024
        height: 800
        color: "#000000"

        Text {
            id: element
            x: 301
            y: 92
            color: "#ffffff"
            text: qsTr("ボタンコンポーネントテスト")
            horizontalAlignment: Text.AlignHCenter
            opacity: 1
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 40
        }
    }

    List_bar {
        id: lb001
        x: 192
        y: 294
    }
}
