import QtQuick 2.12
import gitSample 1.0
import QtQuick.Controls 2.3

Item {
    width: Constants.width
    height: Constants.height

    SwipeView{
        anchors.fill:parent
        currentIndex: 0
        Screen01{}
        Screen02{}
        Screen03{}
        Screen04{}
        Screen05{}
        Screen06{}
    }


}
