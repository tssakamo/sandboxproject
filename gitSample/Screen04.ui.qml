import QtQuick 2.12
import gitSample 1.0
import QtQuick.Controls 2.12
import Qt.SafeRenderer 1.1
import QtQuick.Timeline 1.0

Item {
    id: root
    width: Constants.width
    height: Constants.height

    Rectangle {
        id: rectangle
        width: 1024
        height: 800
        color: "#000000"

        ProgressBar {
            id: progressBar
            x: 185
            y: 294
            width: 717
            height: 82
            font.pointSize: 10
            from: 0
            value: 1
        }
    }

    BusyIndicator {
        id: busyIndicator
        x: 482
        y: 370
    }

    Timeline {
        id: timeline
        animations: [
            TimelineAnimation {
                id: timelineAnimation
                running: true
                to: 5000
                from: 0
                loops: -1
                duration: 5000
            }
        ]
        enabled: true
        endFrame: 5000
        startFrame: 0

        KeyframeGroup {
            target: progressBar
            property: "value"
            Keyframe {
                frame: 0
                value: 1
            }

            Keyframe {
                frame: 2500
                value: 0
            }

            Keyframe {
                frame: 5000
                value: 1
            }
        }
    }
}

/*##^##
Designer {
    D{i:4}
}
##^##*/

