import QtQuick 2.12
import gitSample 1.0

Item {
    width: Constants.width
    height: Constants.height
    ListModel {

        // このリストに名前をつける
        id: ls

        // リストの要素(ListElement内の変数は自由かもしれない)
        ListElement {

            name: "白菜"
            cost: 124
        }
        ListElement {
            name: "なす"
            cost: 98
        }
        ListElement {
            name: "きゅうり"
            cost: 120
        }
        ListElement {
            name: "玉ねぎ"
            cost: 130
        }

    }

    // リストを見るためのもの
    ListView {
        anchors.fill: parent

        // 先ほどのListModelをつかう
        model: ls

        // 要素をRowに移譲して整えたりする
        delegate: Row {
            spacing: 10
            Text { text: "種類: "+ name }
            Text { text: "価格: "+ cost +"円" }
        }
    }
    MouseArea {
        anchors.fill: parent

        // Keyboardからの入力を受けるためフォーカスを当てておく
        focus: true

        // クリックするとトマト追加
        onClicked: ls.append({"name":"トマト","cost": 168})
        // Upボタンで最後尾の要素削除
        Keys.onUpPressed: ls.remove(ls.count-1)
        // Downボタンで全部削除
        Keys.onDownPressed: ls.clear()
    }
}

